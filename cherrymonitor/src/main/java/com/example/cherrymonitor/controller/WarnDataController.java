package com.example.cherrymonitor.controller;

import com.example.cherrymonitor.entity.WarnData;
import com.example.cherrymonitor.service.WarnDataService;
import com.example.cherrymonitor.utils.PageResult;
import com.example.cherrymonitor.utils.Result;
import com.example.cherrymonitor.utils.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/warnData")
public class WarnDataController {
    @Autowired
    private WarnDataService warnDataService;



    /**
     * 查询全部数据
     * @return
     */
    @RequestMapping(method= RequestMethod.GET)
    public Result findAll(){
        return new Result(true, StatusCode.OK,"查询成功",warnDataService.findAll());
    }

    /**
     * 根据ID查询
     * @param id ID
     * @return
     */
    @RequestMapping(value="/{id}",method= RequestMethod.GET)
    public Result findById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",warnDataService.findById(id));
    }


    /**
     * 分页+多条件查询
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<WarnData> pageList = warnDataService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<WarnData>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",warnDataService.findSearch(searchMap));
    }

    /**
     * 根据条件查询，将害虫名一样的放一起
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/findOredrByPestName",method = RequestMethod.POST)
    public Result findOredrByPestName( @RequestBody Map searchMap){
        List<WarnData> search = warnDataService.findSearch(searchMap);
        Map<String,List<WarnData>> map =  new HashMap<>();
        for (WarnData w: search
                ) {
            if(!map.containsKey(w.getPestName())){
                ArrayList<WarnData> arr = new ArrayList<>();
                arr.add(w);
                map.put(w.getPestName(),arr);
            }else{
                map.get(w.getPestName()).add(w);
            }
        }
        return new Result(true,StatusCode.OK,"查询成功",map);
    }

    /**
     * 增加
     * @param warnData
     */
    @RequestMapping(method= RequestMethod.POST)
    public Result add(@RequestBody WarnData warnData  ){
        warnDataService.add(warnData);
        return new Result(true, StatusCode.OK,"增加成功");
    }

    /**
     * 修改
     * @param warnData
     */
    @RequestMapping(value="/{id}",method= RequestMethod.PUT)
    public Result update(@RequestBody WarnData warnData, @PathVariable String id ){
        warnData.setId(id);
        warnDataService.update(warnData);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    /**
     * 删除
     * @param id
     */
    @RequestMapping(value="/{id}",method= RequestMethod.DELETE)
    public Result delete(@PathVariable String id ){
        warnDataService.deleteById(id);
        return new Result(true,StatusCode.OK,"删除成功");
    }
}
