package com.example.cherrymonitor.controller;

import com.example.cherrymonitor.entity.Pesticides;
import com.example.cherrymonitor.service.PesticidesService;
import com.example.cherrymonitor.utils.PageResult;
import com.example.cherrymonitor.utils.Result;
import com.example.cherrymonitor.utils.StatusCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/pesticides")
public class PesticidesController {
    @Autowired
    private PesticidesService pesticidesService;

    /**
     * 查询全部数据
     * @return
     */
    @RequestMapping(method= RequestMethod.GET)
    public Result findAll(){
        return new Result(true, StatusCode.OK,"查询成功",pesticidesService.findAll());
    }

    /**
     * 根据ID查询
     * @param id ID
     * @return
     */
    @RequestMapping(value="/{id}",method= RequestMethod.GET)
    public Result findById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",pesticidesService.findById(id));
    }


    /**
     * 分页+多条件查询
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<Pesticides> pageList = pesticidesService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Pesticides>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true,StatusCode.OK,"查询成功",pesticidesService.findSearch(searchMap));
    }

    /**
     * 增加
     * @param pesticides
     */
    @RequestMapping(method= RequestMethod.POST)
    public Result add(@RequestBody Pesticides pesticides  ){
        pesticidesService.add(pesticides);
        return new Result(true, StatusCode.OK,"增加成功");
    }

    /**
     * 修改
     * @param pesticides
     */
    @RequestMapping(value="/{id}",method= RequestMethod.PUT)
    public Result update(@RequestBody Pesticides pesticides, @PathVariable String id ){
        pesticides.setId(id);
        pesticidesService.update(pesticides);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    /**
     * 删除
     * @param id
     */
    @RequestMapping(value="/{id}",method= RequestMethod.DELETE)
    public Result delete(@PathVariable String id ){
        pesticidesService.deleteById(id);
        return new Result(true,StatusCode.OK,"删除成功");
    }
}
