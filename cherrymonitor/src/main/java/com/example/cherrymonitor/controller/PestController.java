package com.example.cherrymonitor.controller;

import com.example.cherrymonitor.entity.Pest;
import com.example.cherrymonitor.service.PestService;
import com.example.cherrymonitor.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/pest")
public class PestController {

    @Autowired
    private PestService pestService;

    /**
     * 查询全部数据
     * @return
     */
    @RequestMapping(method= RequestMethod.GET)
    public Result findAll(){
        return new Result(true, StatusCode.OK,"查询成功",pestService.findAll());
    }

    /**
     * 根据ID查询
     * @param id ID
     * @return
     */
    @RequestMapping(value="/{id}",method= RequestMethod.GET)
    public Result findById(@PathVariable String id){
        return new Result(true,StatusCode.OK,"查询成功",pestService.findById(id));
    }


    /**
     * 分页+多条件查询
     * @param searchMap 查询条件封装
     * @param page 页码
     * @param size 页大小
     * @return 分页结果
     */
    @RequestMapping(value="/search/{page}/{size}",method=RequestMethod.POST)
    public Result findSearch(@RequestBody Map searchMap , @PathVariable int page, @PathVariable int size){
        Page<Pest> pageList = pestService.findSearch(searchMap, page, size);
        return  new Result(true,StatusCode.OK,"查询成功",  new PageResult<Pest>(pageList.getTotalElements(), pageList.getContent()) );
    }

    /**
     * 根据条件查询
     * @param searchMap
     * @return
     */
    @RequestMapping(value="/search",method = RequestMethod.POST)
    public Result findSearch( @RequestBody Map searchMap){
        return new Result(true, StatusCode.OK,"查询成功",pestService.findSearch(searchMap));
    }

    /**
     * 增加
     * @param pest
     */
    @RequestMapping(method= RequestMethod.POST)
    public Result add(@RequestBody Pest pest  ){
        pestService.add(pest);
        return new Result(true,StatusCode.OK,"增加成功");
    }

    /**
     * 修改
     * @param pest
     */
    @RequestMapping(value="/{id}",method= RequestMethod.PUT)
    public Result update(@RequestBody Pest pest, @PathVariable String id ){
        pest.setId(id);
        pestService.update(pest);
        return new Result(true,StatusCode.OK,"修改成功");
    }

    /**
     * 删除
     * @param id
     */
    @RequestMapping(value="/{id}",method= RequestMethod.DELETE)
    public Result delete(@PathVariable String id ){
        pestService.deleteById(id);
        return new Result(true,StatusCode.OK,"删除成功");
    }
}
