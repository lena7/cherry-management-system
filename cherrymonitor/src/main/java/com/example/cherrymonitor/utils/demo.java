package com.example.cherrymonitor.utils;

import java.util.Arrays;

public class demo {
    public static void main(String[] args) {
//        int[] sums = new int[]{1,3,5,2,10};
//        System.out.println(thirdMax(sums));
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);

        System.out.println(Integer.MAX_VALUE+1);
        System.out.println(Integer.MIN_VALUE-1);

    }
    public static int thirdMax(int[] nums) {
        Arrays.sort(nums);
        if(nums.length<3){
            return nums[nums.length-1];
        }
        return nums[nums.length-3];

    }
}
