package com.example.cherrymonitor.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 预测数据
 */
@Entity
@Table(name = "warn_data")
public class WarnData implements Serializable{
    @Id
    private String id;
    private String equipmentId;
    private String pestName;
    private String confidence;
    private Integer harm_level;
    private Integer number;
    private String remark;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
    private Date colletionTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getPestName() {
        return pestName;
    }

    public void setPestName(String pestName) {
        this.pestName = pestName;
    }

    public String getConfidence() {
        return confidence;
    }

    public void setConfidence(String confidence) {
        this.confidence = confidence;
    }

    public Integer getHarm_level() {
        return harm_level;
    }

    public void setHarm_level(Integer harm_level) {
        this.harm_level = harm_level;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getColletionTime() {
        return colletionTime;
    }

    public void setColletionTime(Date colletionTime) {
        this.colletionTime = colletionTime;
    }

    @Override
    public String toString() {
        return "WarnData{" +
                "id='" + id + '\'' +
                ", equipmentId='" + equipmentId + '\'' +
                ", pestName='" + pestName + '\'' +
                ", confidence='" + confidence + '\'' +
                ", harm_level=" + harm_level +
                ", number=" + number +
                ", remark='" + remark + '\'' +
                ", colletionTime=" + colletionTime +
                '}';
    }
}
