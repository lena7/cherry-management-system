package com.example.cherrymonitor.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 害虫表
 */
@Entity
@Table(name="pest")
public class Pest implements Serializable{
    @Id
    private String id;

    private String name;        //害虫名称
    private String describe;    //描述
    private String brief;       //简介
    private String picture;     // 图片
    private String appearlaw;   //发生规律
    private String enemy;       //天敌
    private String live;        //生活习性
    private String damage;      //危害特点
    private String solve;       //防止手段
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
    private java.util.Date createTime;   //创建时间
    private String createBy;     //创建者

    //多对多关系映射
    @ManyToMany(mappedBy="pests")
    private Set<Pesticides> pesticides = new HashSet<Pesticides>(0);

    public Set<Pesticides> getPesticides() {
        return pesticides;
    }

    public void setPesticides(Set<Pesticides> pesticides) {
        this.pesticides = pesticides;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAppearlaw() {
        return appearlaw;
    }

    public void setAppearlaw(String appearlaw) {
        this.appearlaw = appearlaw;
    }

    public String getEnemy() {
        return enemy;
    }

    public void setEnemy(String enemy) {
        this.enemy = enemy;
    }

    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = live;
    }

    public String getDamage() {
        return damage;
    }

    public void setDamage(String damage) {
        this.damage = damage;
    }

    public String getSolve() {
        return solve;
    }

    public void setSolve(String solve) {
        this.solve = solve;
    }

    public java.util.Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }


}
