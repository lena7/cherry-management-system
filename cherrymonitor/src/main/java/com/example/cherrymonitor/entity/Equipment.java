package com.example.cherrymonitor.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 设备表
 */
@Entity
@Table(name="equipment")
public class Equipment implements Serializable{
    @Id
    private String id;

    private String equipmentId;     //设备编号
    private String equipmentName;   //设备名称
    private String baseId;          // 基地id
    private String baseName;         //基地名称
    private String videoUrl;         //视频路径
    private String userId;          //登录用户id
    private String username;        //接口所需用户名
    private String password;        //接口所需密码
    private String manufactorId;    //设备厂家
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
    private java.util.Date updateTime;      //添加时间
    private String remarks;         //备注
    private String deleted;         //标记删除(0为删除，1已删除)

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    public String getBaseName() {
        return baseName;
    }

    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getManufactorId() {
        return manufactorId;
    }

    public void setManufactorId(String manufactorId) {
        this.manufactorId = manufactorId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "id='" + id + '\'' +
                ", equipmentId='" + equipmentId + '\'' +
                ", equipmentName='" + equipmentName + '\'' +
                ", baseId='" + baseId + '\'' +
                ", baseName='" + baseName + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", manufactorId='" + manufactorId + '\'' +
                ", updateTime=" + updateTime +
                ", remarks='" + remarks + '\'' +
                ", deleted='" + deleted + '\'' +
                '}';
    }
}
