package com.example.cherrymonitor.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * 农药表
 */
@Entity
@Table(name="pesticides")
public class Pesticides implements Serializable{
    @Id
    private String id;
    private String name;            //
    private String method;         //使用方法
    private Double water_ratio;     //兑水比例
    private String picture;       //图片路径
    private String createBy;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GM+8")
    private Date createTime;
    private String isDelete;
    private String warning;     //使用注意事项
    private String byeffect;    //副作用 危害影响
    private String brief;       //简介 成分表
    private String suiteffect;   //适用场景 使用效果

    //多对多关系映射
    @ManyToMany
    @JsonIgnore  //这个注解不加的话，查询的时候双方会不断调用对方，最终报java.lang.IllegalStateException
    @JoinTable(name="pest_pesticides", //中间表的名称
            joinColumns={@JoinColumn(name="pesticides_id",referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="pest_id",referencedColumnName="id")} )
    private Set<Pest> pests = new HashSet<Pest>(0);

    public Set<Pest> getPests() {
        return pests;
    }

    public void setPests(Set<Pest> pests) {
        this.pests = pests;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Double getWater_ratio() {
        return water_ratio;
    }

    public void setWater_ratio(Double water_ratio) {
        this.water_ratio = water_ratio;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public String getByeffect() {
        return byeffect;
    }

    public void setByeffect(String byeffect) {
        this.byeffect = byeffect;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getSuiteffect() {
        return suiteffect;
    }

    public void setSuiteffect(String suiteffect) {
        this.suiteffect = suiteffect;
    }


}
