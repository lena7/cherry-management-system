package com.example.cherrymonitor.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 危害等级实体类
 */
@Entity
@Table(name="harm_level")
public class HarmLevel implements Serializable{
    @Id
    private String id;
    private Integer level;
    private Integer maxV;
    private Integer minV;
    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMaxV() {
        return maxV;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getMinV() {
        return minV;
    }

    public void setMinV(Integer minV) {
        this.minV = minV;
    }

    public void setMaxV(Integer maxV) {
        this.maxV = maxV;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "HarmLevel{" +
                "id='" + id + '\'' +
                ", level=" + level +
                ", maxV=" + maxV +
                ", minV=" + minV +
                ", remark='" + remark + '\'' +
                '}';
    }
}
