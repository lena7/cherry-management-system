package com.example.cherrymonitor.dao;


import com.example.cherrymonitor.entity.Pest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface PestDao extends JpaRepository<Pest,String>,JpaSpecificationExecutor<Pest> {

}