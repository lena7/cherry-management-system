package com.example.cherrymonitor.dao;

import com.example.cherrymonitor.entity.HarmLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface HarmLevelDao extends JpaRepository<HarmLevel,String>,JpaSpecificationExecutor<HarmLevel> {

}