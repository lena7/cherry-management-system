package com.example.cherrymonitor.dao;

import com.example.cherrymonitor.entity.Pesticides;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface PesticidesDao extends JpaRepository<Pesticides,String>,JpaSpecificationExecutor<Pesticides> {


}
