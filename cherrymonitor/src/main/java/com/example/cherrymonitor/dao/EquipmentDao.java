package com.example.cherrymonitor.dao;

import com.example.cherrymonitor.entity.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


/**
 * 数据访问接口
 * @author Administrator
 *
 */
public interface EquipmentDao extends JpaRepository<Equipment,String>,JpaSpecificationExecutor<Equipment>{


}