package com.example.cherrymonitor.service;

import com.example.cherrymonitor.dao.PestDao;
import com.example.cherrymonitor.entity.Pest;
import com.example.cherrymonitor.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class PestService{
    @Autowired
    private PestDao pestDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部列表
     * @return
     */
    public List<Pest> findAll() {
        return pestDao.findAll();
    }


    /**
     * 条件查询+分页
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Pest> findSearch(Map whereMap, int page, int size) {
        Specification<Pest> specification = createSpecification(whereMap);
        PageRequest pageRequest =  PageRequest.of(page-1, size);
        return pestDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     * @param whereMap
     * @return
     */
    public List<Pest> findSearch(Map whereMap) {
        Specification<Pest> specification = createSpecification(whereMap);
        return pestDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     * @param id
     * @return
     */
    public Pest findById(String id) {
        return pestDao.findById(id).get();
    }

    /**
     * 增加
     * @param pest
     */
    public void add(Pest pest) {
        pest.setId( idWorker.nextId()+"" );
        pestDao.save(pest);
    }

    /**
     * 修改
     * @param pest
     */
    public void update(Pest pest) {
        pestDao.save(pest);
    }

    /**
     * 删除
     * @param id
     */
    public void deleteById(String id) {
        pestDao.deleteById(id);
    }

    /**
     * 动态条件构建
     * @param searchMap
     * @return
     */
    private Specification<Pest> createSpecification(Map searchMap) {

        return new Specification<Pest>() {

            @Override
            public Predicate toPredicate(Root<Pest> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 姓名
                if (searchMap.get("name")!=null && !"".equals(searchMap.get("name"))) {
                    predicateList.add(cb.like(root.get("name").as(String.class), "%"+(String)searchMap.get("name")+"%"));
                }

                return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }
}

