package com.example.cherrymonitor.service;

import com.example.cherrymonitor.dao.EquipmentDao;
import com.example.cherrymonitor.entity.Equipment;
import com.example.cherrymonitor.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class EquipmentService {
    @Autowired
    private EquipmentDao equipmentDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部列表
     * @return
     */
    public List<Equipment> findAll() {
        return equipmentDao.findAll();
    }


    /**
     * 条件查询+分页
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Equipment> findSearch(Map whereMap, int page, int size) {
        Specification<Equipment> specification = createSpecification(whereMap);
        PageRequest pageRequest =  PageRequest.of(page-1, size);
        return equipmentDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     * @param whereMap
     * @return
     */
    public List<Equipment> findSearch(Map whereMap) {
        Specification<Equipment> specification = createSpecification(whereMap);
        return equipmentDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     * @param id
     * @return
     */
    public Equipment findById(String id) {
        return equipmentDao.findById(id).get();
    }

    /**
     * 增加
     * @param equipment
     */
    public void add(Equipment equipment) {
        equipment.setId( idWorker.nextId()+"" );
        equipmentDao.save(equipment);
    }

    /**
     * 修改
     * @param equipment
     */
    public void update(Equipment equipment) {
        equipmentDao.save(equipment);
    }

    /**
     * 删除
     * @param id
     */
    public void deleteById(String id) {
        equipmentDao.deleteById(id);
    }

    /**
     * 动态条件构建
     * @param searchMap
     * @return
     */
    private Specification<Equipment> createSpecification(Map searchMap) {

        return new Specification<Equipment>() {

            @Override
            public Predicate toPredicate(Root<Equipment> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 设备id
                if (searchMap.get("equipmentId")!=null && !"".equals(searchMap.get("equipmentId"))) {
                    predicateList.add(cb.like(root.get("equipmentId").as(String.class), "%"+(String)searchMap.get("equipmentId")+"%"));
                }
                // 设备名称
                if (searchMap.get("equipmentName")!=null && !"".equals(searchMap.get("equipmentName"))) {
                    predicateList.add(cb.like(root.get("equipmentName").as(String.class), "%"+(String)searchMap.get("equipmentName")+"%"));
                }
                // 基地id
                if (searchMap.get("baseId")!=null && !"".equals(searchMap.get("baseId"))) {
                    predicateList.add(cb.like(root.get("baseId").as(String.class), "%"+(String)searchMap.get("baseId")+"%"));
                }
                // 基地名称
                //这里的 % % 被我去掉了，因为不能模糊查询
                if (searchMap.get("baseName")!=null && !"".equals(searchMap.get("baseName"))) {
                    predicateList.add(cb.like(root.get("baseName").as(String.class), (String)searchMap.get("baseName")));
                }
                // 用户id
                if (searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))) {
                    predicateList.add(cb.like(root.get("userId").as(String.class), "%"+(String)searchMap.get("userId")+"%"));
                }
                // 接口所需用户名
                if (searchMap.get("username")!=null && !"".equals(searchMap.get("username"))) {
                    predicateList.add(cb.like(root.get("username").as(String.class), "%"+(String)searchMap.get("username")+"%"));
                }
                // 接口所需密码
                if (searchMap.get("password")!=null && !"".equals(searchMap.get("password"))) {
                    predicateList.add(cb.like(root.get("password").as(String.class), "%"+(String)searchMap.get("password")+"%"));
                }
                // 设备厂商
                if (searchMap.get("manufactorId")!=null && !"".equals(searchMap.get("manufactorId"))) {
                    predicateList.add(cb.like(root.get("manufactorId").as(String.class), "%"+(String)searchMap.get("manufactorId")+"%"));
                }
                // 备注
                if (searchMap.get("remarks")!=null && !"".equals(searchMap.get("remarks"))) {
                    predicateList.add(cb.like(root.get("remarks").as(String.class), "%"+(String)searchMap.get("remarks")+"%"));
                }

                return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }
}
