package com.example.cherrymonitor.service;

import com.example.cherrymonitor.dao.PestDao;
import com.example.cherrymonitor.dao.RoleDao;
import com.example.cherrymonitor.entity.Pest;
import com.example.cherrymonitor.entity.Role;
import com.example.cherrymonitor.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class RoleService{
    @Autowired
    private RoleDao roleDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部列表
     * @return
     */
    public List<Role> findAll() {
        return roleDao.findAll();
    }


    /**
     * 条件查询+分页
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Role> findSearch(Map whereMap, int page, int size) {
        Specification<Role> specification = createSpecification(whereMap);
        PageRequest pageRequest =  PageRequest.of(page-1, size);
        return roleDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     * @param whereMap
     * @return
     */
    public List<Role> findSearch(Map whereMap) {
        Specification<Role> specification = createSpecification(whereMap);
        return roleDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     * @param id
     * @return
     */
    public Role findById(String id) {
        return roleDao.findById(id).get();
    }

    /**
     * 增加
     * @param role
     */
    public void add(Role role) {
        role.setId( idWorker.nextId()+"" );
        roleDao.save(role);
    }

    /**
     * 修改
     * @param role
     */
    public void update(Role role) {
        roleDao.save(role);
    }

    /**
     * 删除
     * @param id
     */
    public void deleteById(String id) {
        roleDao.deleteById(id);
    }

    /**
     * 动态条件构建
     * @param searchMap
     * @return
     */
    private Specification<Role> createSpecification(Map searchMap) {

        return new Specification<Role>() {

            @Override
            public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 登录名
                if (searchMap.get("loginName")!=null && !"".equals(searchMap.get("loginName"))) {
                    predicateList.add(cb.like(root.get("loginName").as(String.class), "%"+(String)searchMap.get("loginName")+"%"));
                }
                // 性别
                if (searchMap.get("sex")!=null && !"".equals(searchMap.get("sex"))) {
                    predicateList.add(cb.like(root.get("sex").as(String.class), "%"+(String)searchMap.get("sex")+"%"));
                }

                // 姓名
                if (searchMap.get("name")!=null && !"".equals(searchMap.get("name"))) {
                    predicateList.add(cb.like(root.get("name").as(String.class), "%"+(String)searchMap.get("name")+"%"));
                }
                // 等级
                if (searchMap.get("level")!=null && !"".equals(searchMap.get("level"))) {
                    predicateList.add(cb.like(root.get("level").as(String.class), "%"+(String)searchMap.get("level")+"%"));
                }
                return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }
}

