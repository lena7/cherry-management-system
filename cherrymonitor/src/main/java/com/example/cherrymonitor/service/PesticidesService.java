package com.example.cherrymonitor.service;

import com.example.cherrymonitor.dao.HarmLevelDao;
import com.example.cherrymonitor.dao.PesticidesDao;
import com.example.cherrymonitor.entity.HarmLevel;
import com.example.cherrymonitor.entity.Pesticides;
import com.example.cherrymonitor.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class PesticidesService {
    @Autowired
    private PesticidesDao pesticidesDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部列表
     * @return
     */
    public List<Pesticides> findAll() {
        return pesticidesDao.findAll();
    }


    /**
     * 条件查询+分页
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<Pesticides> findSearch(Map whereMap, int page, int size) {
        Specification<Pesticides> specification = createSpecification(whereMap);
        PageRequest pageRequest =  PageRequest.of(page-1, size);
        return pesticidesDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     * @param whereMap
     * @return
     */
    public List<Pesticides> findSearch(Map whereMap) {
        Specification<Pesticides> specification = createSpecification(whereMap);
        return pesticidesDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     * @param id
     * @return
     */
    public Pesticides findById(String id) {
        return pesticidesDao.findById(id).get();
    }

    /**
     * 增加
     * @param pesticides
     */
    public void add(Pesticides pesticides) {
        pesticides.setId( idWorker.nextId()+"" );
        pesticidesDao.save(pesticides);
    }

    /**
     * 修改
     * @param pesticides
     */
    public void update(Pesticides pesticides) {
        pesticidesDao.save(pesticides);
    }

    /**
     * 删除
     * @param id
     */
    public void deleteById(String id) {
        pesticidesDao.deleteById(id);
    }

    /**
     * 动态条件构建
     * @param searchMap
     * @return
     */
    private Specification<Pesticides> createSpecification(Map searchMap) {

        return new Specification<Pesticides>() {

            @Override
            public Predicate toPredicate(Root<Pesticides> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 名称
                if (searchMap.get("name")!=null && !"".equals(searchMap.get("name"))) {
                    predicateList.add(cb.like(root.get("name").as(String.class), "%"+(String)searchMap.get("name")+"%"));
                }

                return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }
}

