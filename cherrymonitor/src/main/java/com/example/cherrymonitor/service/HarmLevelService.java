package com.example.cherrymonitor.service;

import com.example.cherrymonitor.dao.HarmLevelDao;
import com.example.cherrymonitor.entity.HarmLevel;
import com.example.cherrymonitor.utils.IdWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HarmLevelService {
    @Autowired
    private HarmLevelDao harmLevelDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部列表
     * @return
     */
    public List<HarmLevel> findAll() {
        return harmLevelDao.findAll();
    }


    /**
     * 条件查询+分页
     * @param whereMap
     * @param page
     * @param size
     * @return
     */
    public Page<HarmLevel> findSearch(Map whereMap, int page, int size) {
        Specification<HarmLevel> specification = createSpecification(whereMap);
        PageRequest pageRequest =  PageRequest.of(page-1, size);
        return harmLevelDao.findAll(specification, pageRequest);
    }


    /**
     * 条件查询
     * @param whereMap
     * @return
     */
    public List<HarmLevel> findSearch(Map whereMap) {
        Specification<HarmLevel> specification = createSpecification(whereMap);
        return harmLevelDao.findAll(specification);
    }

    /**
     * 根据ID查询实体
     * @param id
     * @return
     */
    public HarmLevel findById(String id) {
        return harmLevelDao.findById(id).get();
    }

    /**
     * 增加
     * @param harmLevel
     */
    public void add(HarmLevel harmLevel) {
        harmLevel.setId( idWorker.nextId()+"" );
        harmLevelDao.save(harmLevel);
    }

    /**
     * 修改
     * @param harmLevel
     */
    public void update(HarmLevel harmLevel) {
        harmLevelDao.save(harmLevel);
    }

    /**
     * 删除
     * @param id
     */
    public void deleteById(String id) {
        harmLevelDao.deleteById(id);
    }

    /**
     * 动态条件构建
     * @param searchMap
     * @return
     */
    private Specification<HarmLevel> createSpecification(Map searchMap) {

        return new Specification<HarmLevel>() {

            @Override
            public Predicate toPredicate(Root<HarmLevel> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicateList = new ArrayList<Predicate>();
                // id
                if (searchMap.get("id")!=null && !"".equals(searchMap.get("id"))) {
                    predicateList.add(cb.like(root.get("id").as(String.class), "%"+(String)searchMap.get("id")+"%"));
                }
                // 害虫id
                if (searchMap.get("pestId")!=null && !"".equals(searchMap.get("pestId"))) {
                    predicateList.add(cb.like(root.get("pestId").as(String.class), "%"+(String)searchMap.get("pestId")+"%"));
                }
                // 危害等级
                if (searchMap.get("level")!=null && !"".equals(searchMap.get("level"))) {
                    predicateList.add(cb.like(root.get("level").as(String.class), "%"+(String)searchMap.get("level")+"%"));
                }
                // 最大值
                if (searchMap.get("maxV")!=null && !"".equals(searchMap.get("maxV"))) {
                    predicateList.add(cb.like(root.get("maxV").as(String.class), "%"+(String)searchMap.get("maxV")+"%"));
                }
                // 最小值
                if (searchMap.get("minV")!=null && !"".equals(searchMap.get("minV"))) {
                    predicateList.add(cb.like(root.get("minV").as(String.class), (String)searchMap.get("minV")));
                }

                return cb.and( predicateList.toArray(new Predicate[predicateList.size()]));

            }
        };

    }
}

