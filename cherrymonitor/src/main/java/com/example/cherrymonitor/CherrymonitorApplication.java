package com.example.cherrymonitor;

import com.example.cherrymonitor.utils.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CherrymonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CherrymonitorApplication.class, args);
	}


	@Bean
	public IdWorker idWorkker(){
		return new IdWorker(1, 1);
	}

}
